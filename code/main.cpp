#include <iostream>

#include "FileStructure.h"
#include "Sorter.h"

void CreateTest(Dir* dir, int maxlevel, int level = 0) {
  if (level < maxlevel) {
    dir->addDir("abcd");
    dir->addDir("dbca");
    dir->addDir("aaaf");
    dir->addDir("aaaa");
    dir->addFile("abcd.txt");
    dir->addFile("dbca.txt");
    dir->addFile("aaaf.txt");
    dir->addFile("aaaa.txt");
    level++;
    CreateTest(dir->getFirstSubdir(), maxlevel, level);
  }
}

int main() {
  FileStructure f;
  Dir head;
  Sorter sorter;

#ifdef SMALL
  CreateTest(&head, 3);
#else
  f.loadFile("data/gibberish.bin", head);
#endif

#ifdef PRINTING
  std::cout << head.toString(false) << std::endl << std::endl;
#endif

  sorter.SortRecursive(&head);

  // save sorted data into a new file called sorted.bin
  f.saveFile(head, "sorted.bin");

#ifdef PRINTING
  std::cout << std::endl << std::endl << head.toString(false);
#endif

  return 0;
}
