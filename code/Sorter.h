#ifndef SORTER_H
#define SORTER_H

#include "Dir.h"
#include "File.h"

class Sorter {
public:
  void SortRecursive(Dir *top);

private:
  // Returns pointer to new first element
  template <typename T> T *MergeSort(T *first);

  // Returns pointer to second half
  template <typename T> T *MergeSplit(T *first);

  // Returns pointer to new first element
  template <typename T> T *MergeJoin(T *first, T *second);

  // Insert sort style - slower than merge sort
private:
  // Returns pointer to new first element
  template <typename T> T *InsertSort(T *first);

  // Returns pointer to new first element
  template <typename T> T *Insert(T *first, T *inserted);

  // Utility functions
private:
  template <typename T> void PrintChain(T *first, T *inserted);
};

#endif // SORTER_H