#include "Sorter.h"
#include <ctype.h>
#include <iostream>
#include <string>

void Sorter::SortRecursive(Dir *current) {
  if (current == NULL) {
    return;
  }

  Dir *subDir = current->getFirstSubdir();
  while (subDir != NULL) {
    SortRecursive(subDir);
    subDir = subDir->getNext();
  }

  current->setFirstSubdir(MergeSort(current->getFirstSubdir()));
  current->setFirstFile(MergeSort(current->getFirstFile()));
}

template <typename T> T *Sorter::MergeSort(T *first) {
  if (first == NULL || first->getNext() == NULL) {
    return first;
  }

  T *second = MergeSplit(first);
  first = MergeSort(first);
  second = MergeSort(second);

  return MergeJoin(first, second);
}

template <typename T> T *Sorter::MergeSplit(T *first) {
  T *slow;
  T *fast;
  T *second;

  if (first == NULL || first->getNext() == NULL) {
    second = NULL;
  } else {
    slow = first;
    fast = first->getNext();

    /* Advance 'fast' two nodes, and advance 'slow' one node */
    while (fast != NULL) {
      fast = fast->getNext();
      if (fast != NULL) {
        slow = slow->getNext();
        fast = fast->getNext();
      }
    }

    /* 'slow' is before the midpoint in the list, so split it in two
      at that point. */
    second = slow->getNext();
    slow->setNext(NULL);
  }
  return second;
}

template <typename T> T *Sorter::MergeJoin(T *first, T *second) {
  T *result = NULL;

  /* Base cases */
  if (first == NULL) {
    return second;
  } else if (second == NULL) {
    return first;
  }

  /* Pick either first or second, and recur */
  if (first->getName() <= second->getName()) {
    result = first;
    result->setNext(MergeJoin(first->getNext(), second));
  } else {
    result = second;
    result->setNext(MergeJoin(first, second->getNext()));
  }
  return result;
}

template <typename T> void Sorter::PrintChain(T *first, T *inserted) {
  T *current = first;
  while (current != NULL) {
    if (current == first) {
      std::cout << "^";
    }
    if (current == inserted) {
      std::cout << "|" << current->getName() << "|";
      std::cout.flush();
    } else {
      std::cout << current->getName();
      std::cout.flush();
    }
    std::cout << " >> ";
    std::cout.flush();
    current = current->getNext();
  }
  std::cout << "<END>" << std::endl;
  std::cout.flush();
}

template <typename T> T *Sorter::InsertSort(T *first) {
  // Start a new chain
  T *newFirst = NULL;

  T *current = first;
  while (current != NULL) {
    T *next = current->getNext();
    current->setNext(NULL); // Break the chain
    newFirst = Insert(newFirst, current);
    current = next;
  }

  return newFirst;
}

template <typename T> T *Sorter::Insert(T *first, T *inserted) {
  // getName() returns a string copy. We can (mostly) avoid that for inserted
  std::string insertedName = inserted->getName();

  if (first == NULL) {
    return inserted;
  }

  T *newFirst = first;
  T *prev = NULL;
  T *next = first;

  // Breaks out of loop if inserted is smaller than next
  // We want to insert between prev and next
  while (next != NULL && next->getName().compare(insertedName) < 0) {
    prev = next;
    next = next->getNext();
  }

  // Check if inserting in first place
  if (prev == NULL) {
    newFirst = inserted;
  } else {
    prev->setNext(inserted);
  }

  inserted->setNext(next);

#ifdef PRINTING
  PrintChain(newFirst, inserted);
#endif

  return newFirst;
}
